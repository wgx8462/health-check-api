package com.gb.healthcheckapi.controller;

import com.gb.healthcheckapi.model.MachineRequest;
import com.gb.healthcheckapi.model.MachineStaticsResponse;
import com.gb.healthcheckapi.service.MachineService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/machine")
public class MachineController {
    private final MachineService machineService;

    @PostMapping("/new")
    public String setMachine(@RequestBody MachineRequest request) {
        machineService.setMachine(request);

        return "Ok";
    }

    @GetMapping("/statics")
    public MachineStaticsResponse getStatics() {

        return machineService.getStatics();
    }
}
