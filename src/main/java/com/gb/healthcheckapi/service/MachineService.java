package com.gb.healthcheckapi.service;

import com.gb.healthcheckapi.entity.Machine;
import com.gb.healthcheckapi.model.MachineRequest;
import com.gb.healthcheckapi.model.MachineStaticsResponse;
import com.gb.healthcheckapi.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {
    private final MachineRepository machineRepository;

    public void setMachine(MachineRequest request) {
        Machine addData = new Machine();
        addData.setMachineName(request.getMachineName());
        addData.setMachinePrice(request.getMachinePrice());
        addData.setDateBuy(request.getDateBuy());

        machineRepository.save(addData);
    }

    public MachineStaticsResponse getStatics() {
        MachineStaticsResponse response = new MachineStaticsResponse();

        List<Machine> originList = machineRepository.findAll();

        double totalPrice = 0D;
        double totalPressKg = 0D;
        for (Machine machine : originList) {
            // java 연산자
            totalPrice += machine.getMachinePrice();
            // totalPrice = totalPrice + machine.getMachinePrice();
            totalPressKg += machine.getMachineType().getPressKg();
        }

        double avgPrice = totalPrice / originList.size();

            response.setTotalPrice(totalPrice);
            response.setAveragePrice(avgPrice);
            response.setTotalpressKg(totalPressKg);

            return response;
    }
}
