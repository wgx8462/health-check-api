package com.gb.healthcheckapi.model;

import com.gb.healthcheckapi.enums.HealthStatus;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthStatusChangeRequest {
    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private HealthStatus healthStatus;
}
