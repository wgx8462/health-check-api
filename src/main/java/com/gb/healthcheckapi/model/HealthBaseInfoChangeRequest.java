package com.gb.healthcheckapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthBaseInfoChangeRequest {
    @Column(nullable = false, length = 20)
    private String name;
    @Column(nullable = false)
    private Boolean isChronicDisease;
}
